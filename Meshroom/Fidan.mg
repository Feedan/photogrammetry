{
    "header": {
        "pipelineVersion": "2.2",
        "releaseVersion": "2021.1.0",
        "fileVersion": "1.1",
        "nodesVersions": {
            "FeatureMatching": "2.0",
            "DepthMapFilter": "3.0",
            "StructureFromMotion": "2.0",
            "DepthMap": "2.0",
            "Meshing": "7.0",
            "PrepareDenseScene": "3.0",
            "FeatureExtraction": "1.1",
            "MeshFiltering": "3.0",
            "CameraInit": "4.0",
            "ImageMatching": "2.0",
            "Texturing": "5.0"
        }
    },
    "graph": {
        "CameraInit_1": {
            "nodeType": "CameraInit",
            "position": [
                0,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 29,
                "split": 1
            },
            "uids": {
                "0": "3989a933abca61c053950dbe24bc5fad3cf62c3f"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "viewpoints": [
                    {
                        "viewId": 56711585,
                        "poseId": 56711585,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175614.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:14\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:14\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:14\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"363\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"328000\", \"Exif:SubsecTimeDigitized\": \"328000\", \"Exif:SubsecTimeOriginal\": \"328000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 143381418,
                        "poseId": 143381418,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175654.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:54\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:54\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:54\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"236\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"41000\", \"Exif:SubsecTimeDigitized\": \"41000\", \"Exif:SubsecTimeOriginal\": \"41000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"-nan(ind)\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"\", \"GPS:Latitude\": \"-nan(ind), -nan(ind), -nan(ind)\", \"GPS:LatitudeRef\": \"\", \"GPS:Longitude\": \"-nan(ind), -nan(ind), -nan(ind)\", \"GPS:LongitudeRef\": \"\", \"GPS:TimeStamp\": \"-nan(ind), -nan(ind), -nan(ind)\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 162339943,
                        "poseId": 162339943,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175556.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:56\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:56\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:56\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"313\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"379000\", \"Exif:SubsecTimeDigitized\": \"379000\", \"Exif:SubsecTimeOriginal\": \"379000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 219676558,
                        "poseId": 219676558,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175523.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:23\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:23\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:23\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"369\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"13000\", \"Exif:SubsecTimeDigitized\": \"13000\", \"Exif:SubsecTimeOriginal\": \"13000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 293807523,
                        "poseId": 293807523,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175623.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:23\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:23\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:23\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"481\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"964000\", \"Exif:SubsecTimeDigitized\": \"964000\", \"Exif:SubsecTimeOriginal\": \"964000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 405689868,
                        "poseId": 405689868,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175633.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:33\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:33\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:33\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"328\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"277000\", \"Exif:SubsecTimeDigitized\": \"277000\", \"Exif:SubsecTimeOriginal\": \"277000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 689197900,
                        "poseId": 689197900,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175626.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:26\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:26\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:26\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"422\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"555000\", \"Exif:SubsecTimeDigitized\": \"555000\", \"Exif:SubsecTimeOriginal\": \"555000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 748399584,
                        "poseId": 748399584,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175611.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:11\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:11\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:11\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"447\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"233000\", \"Exif:SubsecTimeDigitized\": \"233000\", \"Exif:SubsecTimeOriginal\": \"233000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 862828989,
                        "poseId": 862828989,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175602.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:02\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:02\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:02\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"363\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"448000\", \"Exif:SubsecTimeDigitized\": \"448000\", \"Exif:SubsecTimeOriginal\": \"448000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 919874127,
                        "poseId": 919874127,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175527.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:27\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:27\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:27\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"313\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"10000\", \"Exif:SubsecTimeDigitized\": \"10000\", \"Exif:SubsecTimeOriginal\": \"10000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 964111873,
                        "poseId": 964111873,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175650.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:50\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:50\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:50\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"178\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"350000\", \"Exif:SubsecTimeDigitized\": \"350000\", \"Exif:SubsecTimeOriginal\": \"350000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 976615104,
                        "poseId": 976615104,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175605.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:05\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:05\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:05\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"369\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"386000\", \"Exif:SubsecTimeDigitized\": \"386000\", \"Exif:SubsecTimeOriginal\": \"386000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1035419681,
                        "poseId": 1035419681,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175620.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:20\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:20\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:20\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"369\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"286000\", \"Exif:SubsecTimeDigitized\": \"286000\", \"Exif:SubsecTimeOriginal\": \"286000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1054319933,
                        "poseId": 1054319933,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175530.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:30\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:30\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:30\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"519\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"328000\", \"Exif:SubsecTimeDigitized\": \"328000\", \"Exif:SubsecTimeOriginal\": \"328000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1178411723,
                        "poseId": 1178411723,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175511.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:11\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:11\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:11\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"375\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"851000\", \"Exif:SubsecTimeDigitized\": \"851000\", \"Exif:SubsecTimeOriginal\": \"851000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1296605400,
                        "poseId": 1296605400,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175559.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:59\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:59\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:59\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"347\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"678000\", \"Exif:SubsecTimeDigitized\": \"678000\", \"Exif:SubsecTimeOriginal\": \"678000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1298941127,
                        "poseId": 1298941127,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175630.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:30\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:30\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:30\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"391\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"47000\", \"Exif:SubsecTimeDigitized\": \"47000\", \"Exif:SubsecTimeOriginal\": \"47000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1322320287,
                        "poseId": 1322320287,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175549.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:49\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:49\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:49\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"278\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"761000\", \"Exif:SubsecTimeDigitized\": \"761000\", \"Exif:SubsecTimeOriginal\": \"761000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1450214295,
                        "poseId": 1450214295,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175646.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:46\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:46\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:46\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"163\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"887000\", \"Exif:SubsecTimeDigitized\": \"887000\", \"Exif:SubsecTimeOriginal\": \"887000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1654738080,
                        "poseId": 1654738080,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175639.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:39\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:39\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:39\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"269\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"100000\", \"Exif:SubsecTimeDigitized\": \"100000\", \"Exif:SubsecTimeOriginal\": \"100000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1686690162,
                        "poseId": 1686690162,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175546.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:46\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:46\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:46\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"294\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"222000\", \"Exif:SubsecTimeDigitized\": \"222000\", \"Exif:SubsecTimeOriginal\": \"222000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1839739568,
                        "poseId": 1839739568,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175537.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:37\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:37\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:37\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"416\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"265000\", \"Exif:SubsecTimeDigitized\": \"265000\", \"Exif:SubsecTimeOriginal\": \"265000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1944108947,
                        "poseId": 1944108947,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175553.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:53\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:53\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:53\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"300\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"264000\", \"Exif:SubsecTimeDigitized\": \"264000\", \"Exif:SubsecTimeOriginal\": \"264000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 1945925089,
                        "poseId": 1945925089,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175520.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:20\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:20\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:20\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"378\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"85000\", \"Exif:SubsecTimeDigitized\": \"85000\", \"Exif:SubsecTimeOriginal\": \"85000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 2002446284,
                        "poseId": 2002446284,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175533.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:33\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:33\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:33\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"375\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"982000\", \"Exif:SubsecTimeDigitized\": \"982000\", \"Exif:SubsecTimeOriginal\": \"982000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 2007296910,
                        "poseId": 2007296910,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175642.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:42\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:42\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:42\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"173\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"675000\", \"Exif:SubsecTimeDigitized\": \"675000\", \"Exif:SubsecTimeOriginal\": \"675000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 2016585044,
                        "poseId": 2016585044,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175517.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:17\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:17\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:17\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"353\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"457000\", \"Exif:SubsecTimeDigitized\": \"457000\", \"Exif:SubsecTimeOriginal\": \"457000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 2043040587,
                        "poseId": 2043040587,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175542.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:55:42\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:55:42\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:55:42\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"403\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"768000\", \"Exif:SubsecTimeDigitized\": \"768000\", \"Exif:SubsecTimeOriginal\": \"768000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    },
                    {
                        "viewId": 2073242893,
                        "poseId": 2073242893,
                        "path": "D:/Projects/photogrammetry/mama/IMG20230109175608.jpg",
                        "intrinsicId": 4220570143,
                        "rigId": -1,
                        "subPoseId": -1,
                        "metadata": "{\"AliceVision:useWhiteBalance\": \"1\", \"DateTime\": \"2023:01:09 17:56:08\", \"Exif:ApertureValue\": \"1.69\", \"Exif:BrightnessValue\": \"-nan(ind)\", \"Exif:ColorSpace\": \"1\", \"Exif:DateTimeDigitized\": \"2023:01:09 17:56:08\", \"Exif:DateTimeOriginal\": \"2023:01:09 17:56:08\", \"Exif:ExifVersion\": \"0220\", \"Exif:ExposureBiasValue\": \"0\", \"Exif:ExposureMode\": \"0\", \"Exif:ExposureProgram\": \"0\", \"Exif:Flash\": \"16\", \"Exif:FlashPixVersion\": \"0100\", \"Exif:FocalLength\": \"5.58\", \"Exif:FocalLengthIn35mmFilm\": \"0\", \"Exif:MaxApertureValue\": \"1.69\", \"Exif:MeteringMode\": \"2\", \"Exif:OffsetTimeOriginal\": \"+05:00\", \"Exif:PhotographicSensitivity\": \"434\", \"Exif:PixelXDimension\": \"3456\", \"Exif:PixelYDimension\": \"4608\", \"Exif:SceneCaptureType\": \"0\", \"Exif:SceneType\": \"0\", \"Exif:SensingMethod\": \"0\", \"Exif:ShutterSpeedValue\": \"5.643\", \"Exif:SubsecTime\": \"244000\", \"Exif:SubsecTimeDigitized\": \"244000\", \"Exif:SubsecTimeOriginal\": \"244000\", \"Exif:WhiteBalance\": \"0\", \"Exif:YCbCrPositioning\": \"1\", \"ExposureTime\": \"0.02\", \"FNumber\": \"1.8\", \"GPS:Altitude\": \"172.199\", \"GPS:AltitudeRef\": \"0\", \"GPS:DateStamp\": \"2023:01:09\", \"GPS:Latitude\": \"53, 38, 14.41\", \"GPS:LatitudeRef\": \"N\", \"GPS:Longitude\": \"55, 53, 46.9\", \"GPS:LongitudeRef\": \"E\", \"GPS:TimeStamp\": \"12, 51, 2\", \"ICCProfile\": \"0, 0, 2, 36, 97, 112, 112, 108, 4, 0, 0, 0, 109, 110, 116, 114, 82, 71, 66, 32, 88, 89, 90, 32, 7, 226, 0, 6, 0, 24, 0, 13, 0, 22, 0, 32, 97, 99, 115, 112, 65, 80, 80, 76, 0, 0, 0, 0, 79, 80, 80, 79, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ... [548 x uint8]\", \"Make\": \"realme\", \"Model\": \"realme 6 Pro\", \"Orientation\": \"1\", \"ResolutionUnit\": \"none\", \"XResolution\": \"72\", \"YResolution\": \"72\", \"jpeg:subsampling\": \"4:2:0\", \"oiio:ColorSpace\": \"sRGB\"}"
                    }
                ],
                "intrinsics": [
                    {
                        "intrinsicId": 4220570143,
                        "pxInitialFocalLength": -1.0,
                        "pxFocalLength": 5562.348047707611,
                        "type": "fisheye4",
                        "width": 3456,
                        "height": 4608,
                        "sensorWidth": -1.0,
                        "sensorHeight": -0.75,
                        "serialNumber": "D:/Projects/photogrammetry/mama_realme_realme 6 Pro",
                        "principalPoint": {
                            "x": 1728.0,
                            "y": 2304.0
                        },
                        "initializationMode": "unknown",
                        "distortionParams": [
                            0.0,
                            0.0,
                            0.0,
                            0.0
                        ],
                        "locked": false
                    }
                ],
                "sensorDatabase": "D:\\Programms\\New Programms\\Meshroom\\Meshroom-2021.1.0-win64\\Meshroom-2021.1.0\\aliceVision\\share\\aliceVision\\cameraSensors.db",
                "defaultFieldOfView": 45.0,
                "groupCameraFallback": "folder",
                "allowedCameraModels": [
                    "pinhole",
                    "radial1",
                    "radial3",
                    "brown",
                    "fisheye4",
                    "fisheye1"
                ],
                "useInternalWhiteBalance": true,
                "viewIdMethod": "metadata",
                "viewIdRegex": ".*?(\\d+)",
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/cameraInit.sfm"
            }
        },
        "FeatureExtraction_1": {
            "nodeType": "FeatureExtraction",
            "position": [
                200,
                0
            ],
            "parallelization": {
                "blockSize": 40,
                "size": 29,
                "split": 1
            },
            "uids": {
                "0": "4b1976c2392d82d9bb0a40c246d14463d7bbafdf"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{CameraInit_1.output}",
                "describerTypes": [
                    "sift",
                    "akaze"
                ],
                "describerPreset": "normal",
                "maxNbFeatures": 0,
                "describerQuality": "normal",
                "contrastFiltering": "GridSort",
                "relativePeakThreshold": 0.01,
                "gridFiltering": true,
                "forceCpuExtraction": true,
                "maxThreads": 0,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "ImageMatching_1": {
            "nodeType": "ImageMatching",
            "position": [
                400,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 29,
                "split": 1
            },
            "uids": {
                "0": "5c5fb6a128874596bd782a4d049bc511ca1d5d30"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{FeatureExtraction_1.input}",
                "featuresFolders": [
                    "{FeatureExtraction_1.output}"
                ],
                "method": "VocabularyTree",
                "tree": "D:\\Programms\\New Programms\\Meshroom\\Meshroom-2021.1.0-win64\\Meshroom-2021.1.0\\aliceVision\\share\\aliceVision\\vlfeat_K80L3.SIFT.tree",
                "weights": "",
                "minNbImages": 200,
                "maxDescriptors": 500,
                "nbMatches": 50,
                "nbNeighbors": 50,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/imageMatches.txt"
            }
        },
        "FeatureMatching_1": {
            "nodeType": "FeatureMatching",
            "position": [
                600,
                0
            ],
            "parallelization": {
                "blockSize": 20,
                "size": 29,
                "split": 2
            },
            "uids": {
                "0": "80f05da61a23df11997a60a54ec46709496a1f94"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{ImageMatching_1.input}",
                "featuresFolders": "{ImageMatching_1.featuresFolders}",
                "imagePairsList": "{ImageMatching_1.output}",
                "describerTypes": "{FeatureExtraction_1.describerTypes}",
                "photometricMatchingMethod": "ANN_L2",
                "geometricEstimator": "acransac",
                "geometricFilterType": "fundamental_matrix",
                "distanceRatio": 0.8,
                "maxIteration": 2048,
                "geometricError": 0.0,
                "knownPosesGeometricErrorMax": 5.0,
                "maxMatches": 0,
                "savePutativeMatches": false,
                "crossMatching": false,
                "guidedMatching": true,
                "matchFromKnownCameraPoses": false,
                "exportDebugFiles": false,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "StructureFromMotion_1": {
            "nodeType": "StructureFromMotion",
            "position": [
                800,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 29,
                "split": 1
            },
            "uids": {
                "0": "394b867016f0734e9023b0ca1cd628491489b489"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{FeatureMatching_1.input}",
                "featuresFolders": "{FeatureMatching_1.featuresFolders}",
                "matchesFolders": [
                    "{FeatureMatching_1.output}"
                ],
                "describerTypes": "{FeatureMatching_1.describerTypes}",
                "localizerEstimator": "acransac",
                "observationConstraint": "Basic",
                "localizerEstimatorMaxIterations": 4096,
                "localizerEstimatorError": 0.0,
                "lockScenePreviouslyReconstructed": false,
                "useLocalBA": true,
                "localBAGraphDistance": 1,
                "maxNumberOfMatches": 0,
                "minNumberOfMatches": 0,
                "minInputTrackLength": 2,
                "minNumberOfObservationsForTriangulation": 2,
                "minAngleForTriangulation": 3.0,
                "minAngleForLandmark": 2.0,
                "maxReprojectionError": 4.0,
                "minAngleInitialPair": 5.0,
                "maxAngleInitialPair": 40.0,
                "useOnlyMatchesFromInputFolder": false,
                "useRigConstraint": true,
                "lockAllIntrinsics": false,
                "filterTrackForks": false,
                "initialPairA": "",
                "initialPairB": "",
                "interFileExtension": ".abc",
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/sfm.abc",
                "outputViewsAndPoses": "{cache}/{nodeType}/{uid0}/cameras.sfm",
                "extraInfoFolder": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "PrepareDenseScene_1": {
            "nodeType": "PrepareDenseScene",
            "position": [
                1000,
                0
            ],
            "parallelization": {
                "blockSize": 40,
                "size": 29,
                "split": 1
            },
            "uids": {
                "0": "6912b630995c0ab6634e25fcf9d546b2c06c510b"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{StructureFromMotion_1.output}",
                "imagesFolders": [],
                "outputFileType": "exr",
                "saveMetadata": true,
                "saveMatricesTxtFiles": false,
                "evCorrection": false,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/",
                "outputUndistorted": "{cache}/{nodeType}/{uid0}/*.{outputFileTypeValue}"
            }
        },
        "DepthMap_1": {
            "nodeType": "DepthMap",
            "position": [
                1200,
                0
            ],
            "parallelization": {
                "blockSize": 3,
                "size": 29,
                "split": 10
            },
            "uids": {
                "0": "2c16a4af2e4ee16f557187dee3a10d08d1c99cc0"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{PrepareDenseScene_1.input}",
                "imagesFolder": "{PrepareDenseScene_1.output}",
                "downscale": 2,
                "minViewAngle": 2.0,
                "maxViewAngle": 70.0,
                "sgmMaxTCams": 10,
                "sgmWSH": 4,
                "sgmGammaC": 5.5,
                "sgmGammaP": 8.0,
                "refineMaxTCams": 6,
                "refineNSamplesHalf": 150,
                "refineNDepthsToRefine": 31,
                "refineNiters": 100,
                "refineWSH": 3,
                "refineSigma": 15,
                "refineGammaC": 15.5,
                "refineGammaP": 8.0,
                "refineUseTcOrRcPixSize": false,
                "exportIntermediateResults": false,
                "nbGPUs": 0,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "DepthMapFilter_1": {
            "nodeType": "DepthMapFilter",
            "position": [
                1400,
                0
            ],
            "parallelization": {
                "blockSize": 10,
                "size": 29,
                "split": 3
            },
            "uids": {
                "0": "ef7cac32d7de1b3cbdddd756ac2e229ca5032c7b"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{DepthMap_1.input}",
                "depthMapsFolder": "{DepthMap_1.output}",
                "minViewAngle": 2.0,
                "maxViewAngle": 70.0,
                "nNearestCams": 10,
                "minNumOfConsistentCams": 3,
                "minNumOfConsistentCamsWithLowSimilarity": 4,
                "pixSizeBall": 0,
                "pixSizeBallWithLowSimilarity": 0,
                "computeNormalMaps": false,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/"
            }
        },
        "Meshing_1": {
            "nodeType": "Meshing",
            "position": [
                1600,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 1,
                "split": 1
            },
            "uids": {
                "0": "5a133df82556ed623d8f849515cfce7e626e998f"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{DepthMapFilter_1.input}",
                "depthMapsFolder": "{DepthMapFilter_1.output}",
                "useBoundingBox": false,
                "boundingBox": {
                    "bboxTranslation": {
                        "x": 0.0,
                        "y": 0.0,
                        "z": 0.0
                    },
                    "bboxRotation": {
                        "x": 0.0,
                        "y": 0.0,
                        "z": 0.0
                    },
                    "bboxScale": {
                        "x": 1.0,
                        "y": 1.0,
                        "z": 1.0
                    }
                },
                "estimateSpaceFromSfM": true,
                "estimateSpaceMinObservations": 3,
                "estimateSpaceMinObservationAngle": 10,
                "maxInputPoints": 50000000,
                "maxPoints": 5000000,
                "maxPointsPerVoxel": 1000000,
                "minStep": 2,
                "partitioning": "singleBlock",
                "repartition": "multiResolution",
                "angleFactor": 15.0,
                "simFactor": 15.0,
                "pixSizeMarginInitCoef": 2.0,
                "pixSizeMarginFinalCoef": 4.0,
                "voteMarginFactor": 4.0,
                "contributeMarginFactor": 2.0,
                "simGaussianSizeInit": 10.0,
                "simGaussianSize": 10.0,
                "minAngleThreshold": 1.0,
                "refineFuse": true,
                "helperPointsGridSize": 10,
                "densify": false,
                "densifyNbFront": 1,
                "densifyNbBack": 1,
                "densifyScale": 20.0,
                "nPixelSizeBehind": 4.0,
                "fullWeight": 1.0,
                "voteFilteringForWeaklySupportedSurfaces": true,
                "addLandmarksToTheDensePointCloud": false,
                "invertTetrahedronBasedOnNeighborsNbIterations": 10,
                "minSolidAngleRatio": 0.2,
                "nbSolidAngleFilteringIterations": 2,
                "colorizeOutput": false,
                "addMaskHelperPoints": false,
                "maskHelperPointsWeight": 1.0,
                "maskBorderSize": 4,
                "maxNbConnectedHelperPoints": 50,
                "saveRawDensePointCloud": false,
                "exportDebugTetrahedralization": false,
                "seed": 0,
                "verboseLevel": "info"
            },
            "outputs": {
                "outputMesh": "{cache}/{nodeType}/{uid0}/mesh.obj",
                "output": "{cache}/{nodeType}/{uid0}/densePointCloud.abc"
            }
        },
        "MeshFiltering_1": {
            "nodeType": "MeshFiltering",
            "position": [
                1800,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 1,
                "split": 1
            },
            "uids": {
                "0": "9848be8d5dfc50a7f3dcd65a38a8ffea7f38f65e"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "inputMesh": "{Meshing_1.outputMesh}",
                "keepLargestMeshOnly": false,
                "smoothingSubset": "all",
                "smoothingBoundariesNeighbours": 0,
                "smoothingIterations": 5,
                "smoothingLambda": 1.0,
                "filteringSubset": "all",
                "filteringIterations": 1,
                "filterLargeTrianglesFactor": 60.0,
                "filterTrianglesRatio": 0.0,
                "verboseLevel": "info"
            },
            "outputs": {
                "outputMesh": "{cache}/{nodeType}/{uid0}/mesh.obj"
            }
        },
        "Texturing_1": {
            "nodeType": "Texturing",
            "position": [
                2000,
                0
            ],
            "parallelization": {
                "blockSize": 0,
                "size": 1,
                "split": 1
            },
            "uids": {
                "0": "20859d7b9ecf44655b4598e7640b8548e6302fc4"
            },
            "internalFolder": "{cache}/{nodeType}/{uid0}/",
            "inputs": {
                "input": "{Meshing_1.output}",
                "imagesFolder": "{DepthMap_1.imagesFolder}",
                "inputMesh": "{MeshFiltering_1.outputMesh}",
                "textureSide": 8192,
                "downscale": 2,
                "outputTextureFileType": "png",
                "unwrapMethod": "Basic",
                "useUDIM": true,
                "fillHoles": false,
                "padding": 5,
                "multiBandDownscale": 4,
                "multiBandNbContrib": {
                    "high": 1,
                    "midHigh": 5,
                    "midLow": 10,
                    "low": 0
                },
                "useScore": true,
                "bestScoreThreshold": 0.1,
                "angleHardThreshold": 90.0,
                "processColorspace": "sRGB",
                "correctEV": false,
                "forceVisibleByAllVertices": false,
                "flipNormals": false,
                "visibilityRemappingMethod": "PullPush",
                "subdivisionTargetRatio": 0.8,
                "verboseLevel": "info"
            },
            "outputs": {
                "output": "{cache}/{nodeType}/{uid0}/",
                "outputMesh": "{cache}/{nodeType}/{uid0}/texturedMesh.obj",
                "outputMaterial": "{cache}/{nodeType}/{uid0}/texturedMesh.mtl",
                "outputTextures": "{cache}/{nodeType}/{uid0}/texture_*.{outputTextureFileTypeValue}"
            }
        }
    }
}